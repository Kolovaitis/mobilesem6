package by.kolovaitis.youtubechannels

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import androidx.preference.PreferenceManager
import by.kolovaitis.youtubechannels.config.mainModule
import com.google.firebase.FirebaseApp
import com.yandex.mapkit.MapKitFactory
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidContext
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.util.*


class CustomApplication : Application() {
    override fun onCreate() {
        MapKitFactory.setApiKey(BuildConfig.YANDEX_MAPKIT_KEY)
        MapKitFactory.initialize(this)

        FirebaseApp.initializeApp(this)

        startKoin(this, listOf(mainModule))
        super.onCreate()
    }
}