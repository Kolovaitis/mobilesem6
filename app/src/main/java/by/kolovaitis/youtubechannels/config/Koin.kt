package by.kolovaitis.youtubechannels.config

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import by.kolovaitis.youtubechannels.data.datasource.FirebaseDatasource
import by.kolovaitis.youtubechannels.data.datasource.impl.FirebaseDatasourceImpl
import by.kolovaitis.youtubechannels.data.repository.impl.FirebaseRepositoryImpl
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import by.kolovaitis.youtubechannels.domain.usecase.*
import by.kolovaitis.youtubechannels.ui.MainViewModel
import by.kolovaitis.youtubechannels.ui.details.DetailsViewModel
import by.kolovaitis.youtubechannels.ui.home.HomeViewModel
import by.kolovaitis.youtubechannels.ui.login.SignInViewModel
import by.kolovaitis.youtubechannels.ui.login.SignUpViewModel
import by.kolovaitis.youtubechannels.ui.settings.SettingsViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val mainModule = module {
    single<SharedPreferences> { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
    single<FirebaseAuth> {
        FirebaseAuth.getInstance()
    }
    single<FirebaseFirestore> {
        FirebaseFirestore.getInstance()
    }
    single<FirebaseStorage> {
        FirebaseStorage.getInstance()
    }
    single<FirebaseDatasource> {
        FirebaseDatasourceImpl(get(), get(), get())
    }

    single<FirebaseRepository> {
        FirebaseRepositoryImpl(get())
    }
    factory { GetUserUsecase(get()) }
    factory { SignInUsecase(get()) }
    factory { SignUpUsecase(get()) }
    factory { SignOutUsecase(get()) }
    factory { GetAllUsersUsecase(get()) }
    factory { GetUserDetailsUsecase(get()) }
    factory { RefactorUserUsecase(get()) }
    factory { AddPhotoUsecase(get()) }
    factory { GetAllPhotosUsecase(get()) }
    factory { IsUserAdminUsecase(get()) }
    factory { SetupVideoUsecase(get()) }

    viewModel<MainViewModel> {
        MainViewModel(get())
    }
    viewModel<SignInViewModel> {
        SignInViewModel(get())
    }
    viewModel<SignUpViewModel> {
        SignUpViewModel(get())
    }
    viewModel<SettingsViewModel> {
        SettingsViewModel(get())
    }
    viewModel<HomeViewModel> {
        HomeViewModel(get())
    }
    viewModel<DetailsViewModel> {
        DetailsViewModel(get(), get(), get(), get(), get(), get())
    }
}