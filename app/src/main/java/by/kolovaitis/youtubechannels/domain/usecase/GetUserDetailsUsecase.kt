package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class GetUserDetailsUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(userId:String) = firebaseRepository.getUserDetails(userId)

}