package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class SignUpUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(email: String, password: String, user: User): FirebaseUser? {
        return firebaseRepository.signUp(email, password, user)
    }
}