package by.kolovaitis.youtubechannels.domain.usecase

import android.net.Uri
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository

class SetupVideoUsecase(private val firebaseRepository: FirebaseRepository) {
    companion object{
        const val prefix = "https://youtu.be/"
    }
    suspend operator fun invoke(videoUrl:String, userId:String){
        val url = if(videoUrl.contains(prefix)){
            videoUrl.removePrefix(prefix)
        }else{
            videoUrl
        }
        firebaseRepository.setVideoUrl(url, userId)
    }
}