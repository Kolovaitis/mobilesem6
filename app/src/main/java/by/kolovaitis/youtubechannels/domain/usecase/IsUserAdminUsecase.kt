package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository

class IsUserAdminUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke():Boolean {
        return firebaseRepository.getUser()?.email == "admin@admin.com"
    }

}