package by.kolovaitis.youtubechannels.domain.usecase

import android.net.Uri
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository

class AddPhotoUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(uri: Uri, userId:String){
        firebaseRepository.addImage(uri, userId)
    }
}