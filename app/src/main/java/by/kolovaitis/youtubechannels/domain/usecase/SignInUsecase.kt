package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class SignInUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(email: String, password: String): FirebaseUser? {
        return firebaseRepository.signIn(email, password)
    }
}