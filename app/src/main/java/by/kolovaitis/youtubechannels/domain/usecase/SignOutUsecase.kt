package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class SignOutUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke() =
        firebaseRepository.signOut()

}