package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser

class RefactorUserUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(user: User) {
        return firebaseRepository.refactorUser( user)
    }
}