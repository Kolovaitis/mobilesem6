package by.kolovaitis.youtubechannels.domain.models

import android.net.Uri

data class User(
    val id:String?,
    val name: String?,
    val subscriptionCount: Int?,
    val thematics: String?,
    val imageUri: Uri?,
    val place:String?,
    val videoUrl:String?=null,
)