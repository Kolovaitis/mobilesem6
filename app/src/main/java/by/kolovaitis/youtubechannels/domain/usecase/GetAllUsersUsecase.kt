package by.kolovaitis.youtubechannels.domain.usecase

import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository

class GetAllUsersUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke():List<User>{
        return firebaseRepository.getAllUsers()
    }
}