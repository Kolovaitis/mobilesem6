package by.kolovaitis.youtubechannels.domain.repository

import android.net.Uri
import by.kolovaitis.youtubechannels.domain.models.User
import com.google.firebase.auth.FirebaseUser
import java.net.URI

interface FirebaseRepository {
    suspend fun signIn(email: String, password: String): FirebaseUser?
    suspend fun signUp(email: String, password: String, user: User): FirebaseUser?
    suspend fun signOut()
    suspend fun getUser():FirebaseUser?
    suspend fun getAllUsers():List<User>
    suspend fun getUserDetails(userId:String):User
    suspend fun refactorUser(user:User)
    suspend fun addImage(uri: Uri, userId: String)
    suspend fun getAllImages(userId: String):List<Uri?>
    suspend fun setVideoUrl(videoUrl:String, userId: String)
}