package by.kolovaitis.youtubechannels.domain.usecase

import android.net.Uri
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository

class GetAllPhotosUsecase(private val firebaseRepository: FirebaseRepository) {
    suspend operator fun invoke(userId:String):List<Uri?>{
        return firebaseRepository.getAllImages(userId)
    }
}