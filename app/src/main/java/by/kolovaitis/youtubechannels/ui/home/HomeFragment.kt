package by.kolovaitis.youtubechannels.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.databinding.FragmentHomeBinding
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {
    companion object {
        const val SPAN_COUNT = 2
    }

    private val vm: HomeViewModel by sharedViewModel()
    private lateinit var binding: FragmentHomeBinding
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var adapter: UsersRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutManager = GridLayoutManager(context, SPAN_COUNT)
        adapter = UsersRecyclerViewAdapter(vm.users.value ?: emptyList()) {
            findNavController().navigate(
                R.id.action_navigation_home_to_userDetailsFragment,
                Bundle().apply { putString("user", it) })
        }
        binding.rvUsers.layoutManager = layoutManager
        binding.rvUsers.adapter = adapter
        vm.users.observe(viewLifecycleOwner) {
            adapter.refreshData(it)
        }
        vm.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    override fun onStart() {
        super.onStart()
        vm.refreshUsers()
    }
}