package by.kolovaitis.youtubechannels.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.lang.Exception

open class BaseViewModel() : ViewModel() {
    private val _wasError: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val wasError: LiveData<String> get() = _wasError

    protected val _isLoading: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val isLoading: LiveData<Boolean> get() = _isLoading

    fun showError(message:String?) {
        _wasError.postValue(message)
    }

    protected inline fun runSafe(code: () -> Unit) {
        _isLoading.postValue(true)
        try {
            code()
        } catch (e: Exception) {
            showError(e.localizedMessage)
        }
        _isLoading.postValue(false)
    }
}