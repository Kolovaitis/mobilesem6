package by.kolovaitis.youtubechannels.ui.home

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.domain.models.User
import com.squareup.picasso.Picasso

class UsersRecyclerViewAdapter(
    private var users: List<User> = emptyList(),
    private val onClick: (String) -> Unit
) :
    RecyclerView.Adapter<UsersRecyclerViewAdapter.UserViewHolder>() {
    class UserViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView = itemView.findViewById<ImageView>(R.id.imageView)!!
        val name = itemView.findViewById<TextView>(R.id.tvName)!!
        val subscribers = itemView.findViewById<TextView>(R.id.tvSubscribers)!!
        fun setOnClick(clickListener: View.OnClickListener){
            itemView.setOnClickListener (clickListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemView =
            LayoutInflater.from(parent?.context).inflate(R.layout.item_user, parent, false)
        return UserViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = users[position]
        Log.d("RECYCLER_VIEW", user.toString())
        Picasso.get().load(user.imageUri).placeholder(R.drawable.ic_user).into(holder.imageView)
        holder.name.text = user.name
        holder.subscribers.text = user.subscriptionCount.toString()
        holder.setOnClick{
            onClick(user.id ?: "")
        }
    }

    override fun getItemCount() =
        users.size

    fun refreshData(data: List<User>) {
        users = data
        notifyDataSetChanged()
    }
}