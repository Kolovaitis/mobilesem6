package by.kolovaitis.youtubechannels.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.usecase.GetAllUsersUsecase
import by.kolovaitis.youtubechannels.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

class HomeViewModel(private val getAllUsersUsecase: GetAllUsersUsecase) : BaseViewModel() {

    private val _users: MutableLiveData<List<User>> by lazy {
        MutableLiveData()
    }
    val users: LiveData<List<User>> get() = _users

    init {
     refreshUsers()
    }
    fun refreshUsers(){
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                val users = getAllUsersUsecase()
                _users.postValue(users)
            }
        }
    }
}