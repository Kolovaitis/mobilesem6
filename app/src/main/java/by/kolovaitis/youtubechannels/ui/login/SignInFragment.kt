package by.kolovaitis.youtubechannels.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.navigation.fragment.findNavController
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.databinding.SignInFragmentBinding
import by.kolovaitis.youtubechannels.ui.MainActivity
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

class SignInFragment : Fragment() {
    private lateinit var binding: SignInFragmentBinding
    private val vm: SignInViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = SignInFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.email.setText(vm.email.value)
        binding.password.setText(vm.password.value)
        binding.goToSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
        }
        binding.email.addTextChangedListener {
            vm.emailHasChanged(it.toString())
        }
        binding.password.addTextChangedListener {
            vm.passwordHasChanged(it.toString())
        }
        binding.bSignIn.setOnClickListener {
            vm.login()
        }
        with(vm) {
            isButtonActive.observe(viewLifecycleOwner) {
                binding.bSignIn.isEnabled = it
            }
            wasError.observe(viewLifecycleOwner) {
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            }
            successfullyLogin.observe(viewLifecycleOwner) {
                if (it) {
                    startActivity(Intent(context, MainActivity::class.java))
                    activity?.finish()
                }
            }
            isLoading.observe(viewLifecycleOwner) {
                binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        }
    }

}