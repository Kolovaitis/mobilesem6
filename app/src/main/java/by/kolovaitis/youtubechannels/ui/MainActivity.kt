package by.kolovaitis.youtubechannels.ui

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.ui.login.LoginActivity
import by.kolovaitis.youtubechannels.ui.settings.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class MainActivity : AppCompatActivity() {
    private val vm: MainViewModel by viewModel()

    companion object {
        const val LANGUAGE_TAG = "language"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val dayNightTheme = prefs.getString("theme", "default")
        SettingsFragment.changeTheme(dayNightTheme)
        val colorTheme = when(prefs.getString("color", "default")){
            "green"->R.style.Theme_YoutubeChannelsGreen
            "red"->R.style.Theme_YoutubeChannelsRed
            else -> R.style.Theme_YoutubeChannels
        }
        setTheme(colorTheme)
        vm.user.observe(this, {
            if (it == null) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                setContentView(R.layout.activity_main)
                val navView: BottomNavigationView = findViewById(R.id.nav_view)
                val navController = findNavController(R.id.nav_host_fragment)
                // Passing each menu ID as a set of Ids because each
                // menu should be considered as top level destinations.
                val appBarConfiguration = AppBarConfiguration(
                    setOf(
                        R.id.navigation_home,
                        R.id.navigation_map,
                        R.id.navigation_settings
                    )
                )
                setupActionBarWithNavController(navController, appBarConfiguration)
                navView.setupWithNavController(navController)
            }
        })

    }

    override fun attachBaseContext(base: Context?) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(base)
        val savedLocale = prefs.getString(LANGUAGE_TAG, null)
        val conf = Configuration(base?.resources?.configuration)
        if (savedLocale != null) {
            val locale = Locale(savedLocale)
            Locale.setDefault(locale)
            conf.setLocale(locale)
        } else {
            prefs.edit().putString(LANGUAGE_TAG, Locale.getDefault().language).apply()
        }
        super.attachBaseContext(base?.createConfigurationContext(conf))
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}