package by.kolovaitis.youtubechannels.ui.map

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.databinding.FragmentMapBinding
import by.kolovaitis.youtubechannels.ui.home.HomeViewModel
import com.squareup.picasso.Picasso
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.IconStyle
import com.yandex.runtime.ui_view.ViewProvider
import org.koin.android.viewmodel.ext.android.sharedViewModel


class MapFragment : Fragment() {

    private val vm: HomeViewModel by sharedViewModel()
    private lateinit var binding: FragmentMapBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapObjects = binding.mapview.map.mapObjects
        val geocoder = Geocoder(context)

        vm.users.observe(viewLifecycleOwner) {
            mapObjects.clear()
            for (user in it) {
                val userView =
                    LayoutInflater.from(view.context)
                        .inflate(R.layout.item_user_on_map, view as ViewGroup, false)
                val imageView = userView.findViewById<ImageView>(R.id.imageViewProfile)
                Picasso.get().load(user.imageUri).placeholder(R.drawable.ic_user)
                    .into(imageView)
                val point = geocoder.getFromLocationName(user.place, 1)
                if (point.size > 0) {
                    val mapObject = mapObjects.addPlacemark(
                        Point(
                            point[0].latitude,
                            point[0].longitude,
                        ),
                        ViewProvider(userView)
                    )
                    mapObjects.addTapListener { mapObjectClicked, point ->
                        if (mapObject == mapObjectClicked) {
                            findNavController().navigate(
                                R.id.action_navigation_map_to_userDetailsFragment,
                                Bundle().apply { putString("user", user.id) })
                            true
                        } else {
                            false
                        }
                    }
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        binding.mapview.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        binding.mapview.onStop()
        MapKitFactory.getInstance().onStop()
    }
}