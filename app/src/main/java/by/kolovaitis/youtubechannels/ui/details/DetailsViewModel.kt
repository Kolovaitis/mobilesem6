package by.kolovaitis.youtubechannels.ui.details

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.usecase.*
import by.kolovaitis.youtubechannels.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

class DetailsViewModel(
    private val getUserDetailsUsecase: GetUserDetailsUsecase,
    private val refactorUserUsecase: RefactorUserUsecase,
    isUserAdminUsecase: IsUserAdminUsecase,
    private val addPhotoUsecase: AddPhotoUsecase,
    private val getAllPhotosUsecase: GetAllPhotosUsecase,
    private val setupVideoUsecase: SetupVideoUsecase,
) : BaseViewModel() {
    private val _isUserAdmin: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val isUserAdmin: LiveData<Boolean> get() = _isUserAdmin

    private val _images: MutableLiveData<List<Uri?>> by lazy {
        MutableLiveData()
    }

    val images: LiveData<List<Uri?>> get() = _images

    private val _user: MutableLiveData<User> by lazy {
        MutableLiveData()
    }
    val user: LiveData<User> get() = _user

    private val _showVideo: MutableLiveData<String> by lazy {
        MutableLiveData()
    }

    val showVideo: LiveData<String> get() = _showVideo

    private val _showAddVideo: MutableLiveData<Nothing> by lazy {
        MutableLiveData()
    }

    val showAddVideo: LiveData<Nothing> get() = _showAddVideo

    private lateinit var id: String

    fun setId(id: String) {
        this.id = id
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                _user.postValue(getUserDetailsUsecase(userId = id))
                _images.postValue(getAllPhotosUsecase(userId = id))
            }
        }
    }

    init {
        viewModelScope.launch(context = newSingleThreadContext("user")) {
            _isUserAdmin.postValue(isUserAdminUsecase())
        }
    }

    fun showVideo() {
        if (user.value?.videoUrl != null) {
            _showVideo.postValue(user.value?.videoUrl)
        } else {
            _showAddVideo.postValue(null)
        }
    }

    fun addVideo(videoUrl: String) {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                setupVideoUsecase(videoUrl, id)
                _user.postValue(getUserDetailsUsecase(userId = id))
            }
        }
    }

    fun addPhoto(uri: Uri) {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                addPhotoUsecase(uri = uri, userId = id)
                _images.postValue(getAllPhotosUsecase(id))
            }
        }
    }

    fun refactorUser(name: String, thematics: String, subscribers: String, place: String) {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                refactorUserUsecase(
                    User(
                        id = id,
                        name = name,
                        thematics = thematics,
                        subscriptionCount = subscribers.toInt(),
                        place = place,
                        imageUri = user.value?.imageUri,
                        videoUrl = user.value?.videoUrl,
                    )
                )
                _user.postValue(getUserDetailsUsecase(userId = id))
            }
        }
    }
}