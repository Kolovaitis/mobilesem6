package by.kolovaitis.youtubechannels.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import by.kolovaitis.youtubechannels.domain.usecase.GetUserUsecase
import by.kolovaitis.youtubechannels.ui.base.BaseViewModel
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.launch

class MainViewModel(getUserUsecase: GetUserUsecase) : BaseViewModel() {
    private val _user: MutableLiveData<FirebaseUser> = MutableLiveData()
    val user: LiveData<FirebaseUser> get() = _user
    init {
        viewModelScope.launch {
            _user.postValue(getUserUsecase())
        }
    }
}