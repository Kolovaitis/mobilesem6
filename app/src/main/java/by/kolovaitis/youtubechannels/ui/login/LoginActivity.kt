package by.kolovaitis.youtubechannels.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import by.kolovaitis.youtubechannels.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        setupActionBarWithNavController(findNavController(R.id.loginNav), AppBarConfiguration(setOf(R.id.signInFragment, R.id.signUpFragment)))
    }
}