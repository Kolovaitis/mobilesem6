package by.kolovaitis.youtubechannels.ui.details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import by.kolovaitis.youtubechannels.R
import by.kolovaitis.youtubechannels.databinding.FragmentUserDetailsBinding
import by.kolovaitis.youtubechannels.ui.login.SignUpFragment
import by.kolovaitis.youtubechannels.ui.login.SignUpFragment.Companion.PICK_IMAGE_REQUEST
import com.afollestad.materialdialogs.LayoutMode
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.bottomsheets.BottomSheet
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.squareup.picasso.Picasso
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException


private const val ARG_USER = "user"

class UserDetailsFragment : Fragment() {
    private val vm: DetailsViewModel by viewModel()
    private lateinit var binding: FragmentUserDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { vm.setId(it.getString(ARG_USER, "")) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm.user.observe(viewLifecycleOwner) { user ->
            binding.tvName.text = user.name
            Picasso.get().load(user.imageUri).placeholder(R.drawable.ic_user)
                .into(binding.imageView)
            binding.tvDescription.text = getString(R.string.profile_description).format(
                user.thematics,
                user.subscriptionCount,
                user.place,
            )
        }
        vm.isUserAdmin.observe(viewLifecycleOwner) {
            binding.bRefactor.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        vm.showAddVideo.observe(viewLifecycleOwner) {
            showAddVideo()
        }
        vm.showVideo.observe(viewLifecycleOwner) {
            showVideo(it)
        }
        vm.isLoading.observe(viewLifecycleOwner) {
            binding.progressBar.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        vm.images.observe(viewLifecycleOwner) {
            if (it != null && it.isNotEmpty() && it[0] != null)
                Picasso.get().load(it[0]).into(binding.ivGallery)
        }
        binding.bVideo.setOnClickListener {
            vm.showVideo()
        }
        binding.ivGallery.setOnClickListener {
            showImages()
        }
        binding.bAddImage.setOnClickListener {
            addImage()
        }
        binding.bRefactor.setOnClickListener { refactor() }

    }

    private fun showAddVideo() {
        MaterialDialog(requireContext()).show {
            title(R.string.add_video)
            input(
                inputType = InputType.TYPE_TEXT_VARIATION_URI,
            )
            positiveButton {
                val url = it.getInputField().text.toString()
                vm.addVideo(url)
            }
            negativeButton()
        }
    }

    private fun showVideo(url: String) {
        val youtubeView = YouTubePlayerView(requireContext()).apply {
            addYouTubePlayerListener(object :
                AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.loadVideo(url, 0f)
                }
            })
        }
        val layoutParams =
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        youtubeView.layoutParams = layoutParams
        MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT)).show {
            title(R.string.video)
            customView(view = youtubeView)
            positiveButton()
            negativeButton(R.string.change_video) {
                showAddVideo()
                it.cancel()
            }

        }
    }

    private fun showImages() {
        val linearLayout = LinearLayout(context)
        linearLayout.gravity = Gravity.CENTER_HORIZONTAL
        vm.images.value?.forEach { uri ->
            val view = LayoutInflater.from(context).inflate(
                R.layout.image_in_gallery,
                LinearLayout(context).apply { linearLayout.addView(this) })
            Picasso.get().load(uri).into(view.findViewById<ImageView>(R.id.imageView))

        }
        MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT)).show {
            title(R.string.images)
            customView(view = HorizontalScrollView(context).apply { addView(linearLayout) })
            positiveButton()
        }
    }

    private fun addImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(
                intent,
                "Select Image from here..."
            ),
            PICK_IMAGE_REQUEST
        )
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            val uri = data.data
            try {
                uri?.let { vm.addPhoto(it) }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun refactor(){
        MaterialDialog(requireContext(), BottomSheet(LayoutMode.WRAP_CONTENT)).show {
            title(R.string.images)
            customView(R.layout.refactor)
            positiveButton{
                with(it.getCustomView()){
                    vm.refactorUser(
                    name = findViewById<EditText>(R.id.tvName).text.toString(),
                    subscribers = findViewById<EditText>(R.id.tvSubscribersCount).text.toString(),
                    thematics = findViewById<EditText>(R.id.tvThematics).text.toString(),
                    place = findViewById<EditText>(R.id.tvPlace).text.toString(),)
                }
            }
        }.getCustomView().apply {
            findViewById<EditText>(R.id.tvName).setText(vm.user.value?.name)
            findViewById<EditText>(R.id.tvSubscribersCount).setText(vm.user.value?.subscriptionCount.toString())
            findViewById<EditText>(R.id.tvThematics).setText(vm.user.value?.thematics)
            findViewById<EditText>(R.id.tvPlace).setText(vm.user.value?.place)

        }
    }
}
