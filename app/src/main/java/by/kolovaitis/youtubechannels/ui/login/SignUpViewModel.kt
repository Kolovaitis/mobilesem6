package by.kolovaitis.youtubechannels.ui.login

import android.net.Uri
import androidx.lifecycle.*
import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.usecase.SignUpUsecase
import by.kolovaitis.youtubechannels.ui.base.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

class SignUpViewModel(private val signUpUsecase: SignUpUsecase) : BaseViewModel() {
    private val _email: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val email: LiveData<String> get() = _email

    private val _password: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val password: LiveData<String> get() = _password

    private val _displayName: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val displayName: LiveData<String> get() = _displayName

    private val _thematics: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val thematics: LiveData<String> get() = _thematics

    private val _place: MutableLiveData<String> by lazy {
        MutableLiveData()
    }
    val place: LiveData<String> get() = _place

    private val _subscribers: MutableLiveData<Int> by lazy {
        MutableLiveData()
    }
    val subscribers: LiveData<Int> get() = _subscribers

    private val _isButtonActive: MediatorLiveData<Boolean> by lazy {
        MediatorLiveData<Boolean>().apply {
            val observer = Observer<Any> {
                if (_email.value.isNullOrEmpty() ||
                    _password.value.isNullOrEmpty() ||
                    _password.value!!.length < 5 ||
                    _place.value.isNullOrEmpty() ||
                    _thematics.value.isNullOrEmpty() ||
                    _displayName.value.isNullOrEmpty() ||
                    _subscribers.value == null ||
                    isLoading.value == true
                ) {
                    this.postValue(false)
                } else {
                    this.postValue(true)
                }
            }
            this.addSource(_email, observer)
            this.addSource(_password, observer)
            this.addSource(_place, observer)
            this.addSource(_thematics, observer)
            this.addSource(_displayName, observer)
            this.addSource(_subscribers, observer)
            this.addSource(isLoading, observer)

        }
    }
    val isButtonActive: LiveData<Boolean> get() = _isButtonActive


    private val _successfullyLogin: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val successfullyLogin: LiveData<Boolean> get() = _successfullyLogin

    private val _image: MutableLiveData<Uri> by lazy {
        MutableLiveData()
    }
    val image: LiveData<Uri> get() = _image

    fun login() {
        viewModelScope.launch(context = newSingleThreadContext("")) {
            runSafe {
                if (signUpUsecase(
                        email.value!!,
                        password.value!!,
                        User(
                            null,
                            displayName.value,
                            subscribers.value,
                            thematics.value,
                            image.value,
                            place.value,
                        )
                    ) != null
                ) {
                    _successfullyLogin.postValue(true)
                } else {
                    showError("Invalid params")
                }
            }
        }
    }

    fun emailHasChanged(newEmail: String) {
        _email.postValue(newEmail)
    }

    fun passwordHasChanged(newPassword: String) {
        _password.postValue(newPassword)
    }

    fun nameHasChanged(newName: String) {
        _displayName.postValue(newName)
    }

    fun subscribersHasChanged(newCount: Int) {
        _subscribers.postValue(newCount)
    }

    fun thematicsHasChanged(newThematics: String) {
        _thematics.postValue(newThematics)
    }

    fun placeHasChanged(newPlace: String) {
        _place.postValue(newPlace)
    }

    fun selectBitmap(uri: Uri) {
        _image.postValue(uri)
    }
}