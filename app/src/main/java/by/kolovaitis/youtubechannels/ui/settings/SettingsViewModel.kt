package by.kolovaitis.youtubechannels.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import by.kolovaitis.youtubechannels.domain.usecase.SignOutUsecase
import by.kolovaitis.youtubechannels.ui.base.BaseViewModel
import kotlinx.coroutines.launch

class SettingsViewModel(private val signOutUsecase: SignOutUsecase) : BaseViewModel() {
    private val _successfullyLogout: MutableLiveData<Boolean> by lazy {
        MutableLiveData(false)
    }
    val successfullyLogout: LiveData<Boolean> get() = _successfullyLogout
    fun signOut() {
        viewModelScope.launch {
            runSafe {
                signOutUsecase()
                _successfullyLogout.postValue(true)
            }
        }
    }

}