package by.kolovaitis.youtubechannels.data.datasource.impl

import android.net.Uri
import android.util.Log
import by.kolovaitis.youtubechannels.data.datasource.FirebaseDatasource
import by.kolovaitis.youtubechannels.data.models.UserData
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.lang.Exception
import java.util.*

class FirebaseDatasourceImpl(
    private val auth: FirebaseAuth,
    private val db: FirebaseFirestore,
    private val storage: FirebaseStorage
) :
    FirebaseDatasource {
    private val users = db.collection("users")
    private val images = storage.reference
    override suspend fun signIn(email: String, password: String): FirebaseUser? {
        val task = auth.signInWithEmailAndPassword(email, password)
        Tasks.await(task)
        return task.result?.user
    }

    override suspend fun signUp(
        email: String,
        password: String,
        userData: UserData,
        imageUri: Uri?,
    ): FirebaseUser? {
        val task = auth.createUserWithEmailAndPassword(email, password)
        Tasks.await(task)
        val user = task.result?.user
        if (user != null) {
            users.document(user.uid).set(
                UserData(
                    user.uid,
                    name = userData.name,
                    subscribersCount = userData.subscribersCount,
                    thematics = userData.thematics,
                    place = userData.place,
                )
            )
            imageUri?.let { images.child("users/${user.uid}").putFile(it) }
        }
        return user
    }

    override suspend fun signOut() {
        auth.signOut()
    }

    override suspend fun getUser(): FirebaseUser? =
        auth.currentUser

    override suspend fun getUserDetails(userId: String): UserData {
        val task = users.document(userId).get()
        Tasks.await(task)
        return task.result?.toObject(UserData::class.java)
            ?: throw Exception("No such user")
    }

    override suspend fun refactorUserUsecase(data: UserData) {
        users.document(data.id).set(data)
    }

    override suspend fun getAllUsers(): List<UserData>? {
        val task = this.users.get()
        Tasks.await(task)
        return task.result?.toObjects(UserData::class.java)
    }

    override suspend fun getImageUri(path: String): Uri? {
        Log.d("PATH", path)
        val task = images.child(path).downloadUrl
        try {
            Tasks.await(task)
        } catch (e: Exception) {
            return null
        }
        return task.result
    }

    override suspend fun getUserImageUri(userId: String): Uri? {
        return getImageUri("users/$userId")
    }

    override suspend fun loadImage(imageUri: Uri, userId: String) {
        images.child("$userId/${UUID.randomUUID()}").putFile(imageUri)
    }

    override suspend fun getAllUserImages(userId: String): List<Uri?> {
        val listTask = images.child(userId).listAll()
        Tasks.await(listTask)
        return listTask.result?.items?.map { ref ->
            val itemTask = ref.downloadUrl
            Tasks.await(itemTask)
            itemTask.result
        } ?: emptyList<Uri>()
    }

    override suspend fun upadateVideoUrl(videoUrl: String, userId: String) {
        val currentData = getUserDetails(userId)
        val newData = currentData.copy(videoUrl = videoUrl)
        refactorUserUsecase(newData)
    }


}