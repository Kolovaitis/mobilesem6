package by.kolovaitis.youtubechannels.data.models

data class UserData(
    val id: String="",
    val name: String = "",
    val subscribersCount: Int = 0,
    val thematics: String = "",
    val place:String = "",
    val videoUrl:String? = null,
)