package by.kolovaitis.youtubechannels.data.datasource

import android.net.Uri
import by.kolovaitis.youtubechannels.data.models.UserData
import com.google.firebase.auth.FirebaseUser

interface FirebaseDatasource {
    suspend fun signIn(email: String, password: String): FirebaseUser?
    suspend fun signUp(email: String, password: String, userData:UserData, imageUri: Uri?): FirebaseUser?
    suspend fun signOut()
    suspend fun getUser():FirebaseUser?
    suspend fun getUserDetails(userId: String):UserData
    suspend fun refactorUserUsecase(data:UserData)
    suspend fun getAllUsers():List<UserData>?
    suspend fun getImageUri(path: String): Uri?
    suspend fun getUserImageUri(userId:String): Uri?
    suspend fun loadImage(imageUri:Uri, userId: String)
    suspend fun getAllUserImages(userId:String):List<Uri?>
    suspend fun upadateVideoUrl(videoUrl:String, userId:String)
}