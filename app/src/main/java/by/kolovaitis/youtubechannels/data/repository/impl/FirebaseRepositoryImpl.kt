package by.kolovaitis.youtubechannels.data.repository.impl

import android.net.Uri
import by.kolovaitis.youtubechannels.data.datasource.FirebaseDatasource
import by.kolovaitis.youtubechannels.data.models.UserData
import by.kolovaitis.youtubechannels.domain.models.User
import by.kolovaitis.youtubechannels.domain.repository.FirebaseRepository
import com.google.firebase.auth.FirebaseUser
import java.net.URI

class FirebaseRepositoryImpl(private val firebaseDatasource: FirebaseDatasource) :
    FirebaseRepository {
    override suspend fun signIn(email: String, password: String) =
        firebaseDatasource.signIn(email, password)

    override suspend fun signUp(
        email: String,
        password: String,
        user: User
    ) = with(user) {
        firebaseDatasource.signUp(
            email,
            password,
            userData = UserData(
                name = name ?: "",
                subscribersCount = subscriptionCount ?: 0,
                thematics = thematics ?: "",
                place = place ?: "",
            ),
            imageUri = imageUri
        )
    }

    override suspend fun signOut() =
        firebaseDatasource.signOut()

    override suspend fun getUser() = firebaseDatasource.getUser()
    override suspend fun getAllUsers(): List<User> {
        return firebaseDatasource.getAllUsers()?.map {
            User(
                name = it.name,
                id = it.id,
                thematics = it.thematics,
                subscriptionCount = it.subscribersCount,
                imageUri = firebaseDatasource.getUserImageUri(it.id),
                place = it.place,
                videoUrl = it.videoUrl
            )
        } ?: emptyList()
    }

    override suspend fun getUserDetails(userId: String): User {
        return with(firebaseDatasource.getUserDetails(userId)) {
            User(
                name = name,
                id = id,
                thematics = thematics,
                subscriptionCount = subscribersCount,
                imageUri = firebaseDatasource.getUserImageUri(id),
                place = place,
                videoUrl = videoUrl
            )
        }
    }

    override suspend fun refactorUser(user: User) {
        firebaseDatasource.refactorUserUsecase(
            with(user) {
                UserData(
                    name = name?:"",
                    id = id!!,
                    thematics = thematics?:"",
                    subscribersCount = subscriptionCount?:0,
                    place = place?:"",
                    videoUrl = videoUrl
                )
            }
        )
    }

    override suspend fun addImage(uri: Uri, userId: String) {
        firebaseDatasource.loadImage(imageUri = uri, userId=userId)
    }

    override suspend fun getAllImages(userId: String): List<Uri?> {
        return firebaseDatasource.getAllUserImages(userId)
    }

    override suspend fun setVideoUrl(videoUrl: String, userId: String) {
        firebaseDatasource.upadateVideoUrl(videoUrl, userId)
    }
}